| Usage | Requirement |
| :--- | :--- |
| [![Nextflow](https://img.shields.io/badge/code-Nextflow-blue?style=plastic)](https://www.nextflow.io/) | [![Dependencies: Nextflow Version](https://img.shields.io/badge/Nextflow-v23.04.4.5881-blue?style=plastic)](https://github.com/nextflow-io/nextflow) |
| [![License: GPL-3.0](https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-green?style=plastic)](https://www.gnu.org/licenses) | [![Dependencies: Apptainer Version](https://img.shields.io/badge/Apptainer-v1.2.3-blue?style=plastic)](https://github.com/apptainer/apptainer) |
| | [![Dependencies: Graphviz Version](https://img.shields.io/badge/Graphviz-v2.42.2-blue?style=plastic)](https://www.graphviz.org/download/) |

<br /><br />
## TABLE OF CONTENTS


   - [AIM](#aim)
   - [WARNING](#warning)
   - [CONTENT](#content)
   - [INPUT](#input)
   - [HOW TO RUN](#how-to-run)
   - [OUTPUT](#output)
   - [VERSIONS](#versions)
   - [LICENCE](#licence)
   - [CITATION](#citation)
   - [CREDITS](#credits)
   - [ACKNOWLEDGEMENTS](#Acknowledgements)
   - [WHAT'S NEW IN](#what's-new-in)

<br /><br />
## AIM

- MICMAC stands for **Metagenome Inter Contact Maps Analysis using Chi2**.
- Use a sparse data frame with the number of contacts between Metagenome Assembled genomes (MAG). Several bins/contigs per MAG (i.e., several lines for the same MGA1 MGA2 contacts in the data frame) are accepted.
- Remove the intra contact maps (contacts of a MAG with itself).
- Compute a Chi2 score for each inter contact map, based on the null hypothesis that the counting of a MAG is randomly distributed in each of its inter contact maps.


<br /><br />
## WARNINGS

- Each chi2 score is computed on a single inter contact map (single cell of the contacts matrix) while the $chi^2(df)$ probability distribution curve used to determine the corresponding p value is for the sum of all the chi2 scores of all the inter contact maps (chi2 score total). This strongly lower the sensitivity of the tests but increase the specificity.
- P value significances must be used with caution to claim that 2 MAGs are indeed in contact in the natural environment.


<br /><br />
## CONTENT

| Files and folder | Description |
| :--- | :--- |
| **main.nf** | File that can be executed using a linux terminal, a MacOS terminal or Windows 10 WSL2. |
| **nextflow.config** | Parameter settings for the *main.nf* file. Users have to open this file, set the desired settings and save these modifications before execution. |
| **bin folder** | Contains files required by the *main.nf* file. |
| **Licence.txt** | Licence of the release. |


<br /><br />
## INPUT

| Required files |
| :--- |
| A folder containing files (.txt, .tsv) containing a sparse counting table. Each file is analyzed independently from the others. |

<br />

The dataset used in the *nextflow.config* file, as example, is available at https://zenodo.org/records/12790703.

<br />

| File name | Description |
| :--- | :--- |
| **table1.txt** | First sparse counting table of a metagenome contact maps. |
| **table2.tsv** | Second sparse counting table of a metagenome contact maps. |

<br />

Each file must be structured like this:
- Space of tabulation separated.
- No header.
- And:

<br />

| Column | Description |
| :--- | :--- |
| **column 1** | Contig name of MAG 1. |
| **column 2** | Contig size. |
| **column 3** | Name of MAG 1. |
| **column 4** | Contig name of MAG 2. |
| **column 5** | Contig size. |
| **column 6** | Name of MAG 2. |

<br /><br />
## HOW TO RUN

### 1. Prerequisite

Installation of:<br />
[nextflow DSL2](https://github.com/nextflow-io/nextflow)<br />
[Graphviz](https://www.graphviz.org/download/), `sudo apt install graphviz` for Linux ubuntu<br />
[Apptainer](https://github.com/apptainer/apptainer)<br />

<br /><br />
### 2. Local running (personal computer)


#### 2.1. *main.nf* file in the personal computer

- Mount a server if required:

<pre>
DRIVE="Z" # change the letter to fit the correct drive
sudo mkdir /mnt/share
sudo mount -t drvfs $DRIVE: /mnt/share
</pre>

Warning: if no mounting, it is possible that nextflow does nothing, or displays a message like:
<pre>
Launching `main.nf` [loving_morse] - revision: d5aabe528b
/mnt/share/Users
</pre>

- Run the following command from where the *main.nf* and *nextflow.config* files are (example: \\wsl$\Ubuntu-20.04\home\gael):

<pre>
nextflow run main.nf -c nextflow.config
</pre>

with -c to specify the name of the config file used.

<br /><br />
#### 2.3. *main.nf* file in the public gitlab repository

Run the following command from where you want the results:

<pre>
nextflow run -hub pasteur gmillot/micmac -r v1.0.0
</pre>

<br /><br />
### 3. Distant running (example with the Pasteur cluster)

#### 3.1. Pre-execution

Copy-paste this after having modified the EXEC_PATH variable:

<pre>
EXEC_PATH="/pasteur/helix/projects/BioIT/gmillot/micmac" # where the bin folder of the main.nf script is located
export CONF_BEFORE=/opt/gensoft/exe # on maestro

export JAVA_CONF=java/13.0.2
export JAVA_CONF_AFTER=bin/java # on maestro
export APP_CONF=apptainer/1.3.2
export APP_CONF_AFTER=bin/apptainer # on maestro
export GIT_CONF=git/2.39.1
export GIT_CONF_AFTER=bin/git # on maestro
export GRAPHVIZ_CONF=graphviz/2.42.3
export GRAPHVIZ_CONF_AFTER=bin/graphviz # on maestro

MODULES="${CONF_BEFORE}/${JAVA_CONF}/${JAVA_CONF_AFTER},${CONF_BEFORE}/${APP_CONF}/${APP_CONF_AFTER},${CONF_BEFORE}/${GIT_CONF}/${GIT_CONF_AFTER}/${GRAPHVIZ_CONF}/${GRAPHVIZ_CONF_AFTER}"
cd ${EXEC_PATH}
chmod 755 ${EXEC_PATH}/bin/*.* # not required if no bin folder
module load ${JAVA_CONF} ${APP_CONF} ${GIT_CONF} ${GRAPHVIZ_CONF}
</pre>

<br /><br />
#### 3.2. *main.nf* file in a cluster folder

Modify the second line of the code below, and run from where the *main.nf* and *nextflow.config* files are (which has been set thanks to the EXEC_PATH variable above):

<pre>
HOME_INI=$HOME
HOME="${HELIXHOME}/micmac/" # $HOME changed to allow the creation of .nextflow into /$HELIXHOME/micmac/, for instance. See NFX_HOME in the nextflow software script
nextflow run --modules ${MODULES} main.nf -c nextflow.config
HOME=$HOME_INI
</pre>

<br /><br />
#### 3.3. *main.nf* file in the public gitlab repository

Modify the first and third lines of the code below, and run (results will be where the EXEC_PATH variable has been set above):

<pre>
VERSION="v1.0"
HOME_INI=$HOME
HOME="${HELIXHOME}/micmac/" # $HOME changed to allow the creation of .nextflow into /$HELIXHOME/micmac/, for instance. See NFX_HOME in the nextflow software script
nextflow run --modules ${MODULES} -hub pasteur gmillot/micmac -r $VERSION -c $HOME/nextflow.config
HOME=$HOME_INI
</pre>

<br /><br />
### 4. Error messages and solutions

#### Message 1
```
Unknown error accessing project `gmillot/micmac` -- Repository may be corrupted: /pasteur/sonic/homes/gmillot/.nextflow/assets/gmillot/micmac
```

Purge using:
<pre>
rm -rf /pasteur/sonic/homes/gmillot/.nextflow/assets/gmillot*
</pre>

#### Message 2
```
WARN: Cannot read project manifest -- Cause: Remote resource not found: https://gitlab.pasteur.fr/api/v4/projects/gmillot%2Fmicmac
```

Contact Gael Millot (distant repository is not public).

#### Message 3

```
permission denied
```

Use chmod to change the user rights. Example linked to files in the bin folder: 
```
chmod 755 bin/*.*
```


<br /><br />
## OUTPUT


An example of results obtained with the dataset is present at this address: https://zenodo.org/records/12790703/files/micmac_1721578178.zip
<br /><br />
| micmac_<UNIQUE_ID> folder | Description |
| :--- | :--- |
| **reports** | Folder containing all the reports of the different processes, including the *nextflow.config* file used. |
| **<FILE_NAME>_micmac.tsv** | Statistical analysis of the corresponding file.<br />Column description: <br /><ul><li>X_MAG: Name of MAG 1.<br /></li><li>Y_MAG: Name of MAG 2.<br /></li><li>COUNT: Counting (number of reads).<br /></li><li>ni.: Total number of reads in all inter contact maps of MAG 1.<br /></li><li>pi.: Proportion of reads in MAG 1 among the total number of reads in all inter contact maps ($p_{i.} = \frac{n_{i.}}{n}$).<br /></li><li>n.j: Total number of reads in all inter contact maps of MAG 2.<br /></li><li>p.j: Proportion of reads in MAG 2 among the total number of reads in all inter contact maps ($p_{.j} = \frac{n_{.j}}{n}$).<br /></li><li>n: Total number of reads in all inter contact maps.<br /></li><li>pij: Proportion of reads in the MAG 1 and MAG 2 inter contact map, among the total number of reads in inter contact maps ($p_{ij} = \frac{COUNT}{n}$).<br /></li><li>pi.\_p.j: Probability to have ${P(MAG_{1} \cap MAG_{2})}$ number of reads, equal to $p_{i.} \times p_{.j}$ under the $H_{0}$ assumption of independ events (no physical links between the two MAGs).<br /></li><li>tij: Theoretical number of reads in the MAG 1 and MAG 2 inter contact map, under the $H_{0}$ assumption ($t_{ij}=pi.\_p.j \times n$).<br /></li><li>n\_score: optional score ${\frac{COUNT}{\sqrt{n_{i.} \times n_{.j}}}}$ (deprecated but kept for comparison).<br /></li><li>chi2\_p: $chi^2$ score between 0 and 1 ($chi2\_p=\frac{(pij - pi.\_p.j)^2}{pi.\_p.j}$). A 0 value means no difference between pi.\_p.j and pij, meaning that under $H_{0}$, the pij observed is due to randomness, i.e., noise, i.e., no physical contact between MAG 1 and MAG 2. A 1 value means maximal difference between pi.\_p.j and pij. This difference can be due to sampling fluctuation but also to real physical link between MAG 1 and MAG 2. It thus implies that the $H_{1}$ hypothesis, alternative to $H_{0}$, is $pij \gt pi.\_p.j = pij \times n \gt pi.\_p.j \times n = COUNT \gt tij$, because the number of reads in an inter contact map, lower than what is expected under $H_{0}$ means nothing, while upper means "physical contact between MAG 1 and MAG 2 above noise".<br /></li><li>res: like chi2 (below) but without the square ($res=\frac{(pij - pi.\_p.j)}{pi.\_p.j} \times n$), in order to define if the (pij - pi.\_p.j) difference is negative or positive. A negative value indicates that pij < pi.\_p.j.<br /></li><li>chi2: $chi^2$ score ($chi2=chi2\_p \times n$).<br /></li><li>df: degree of freedom (number of inter contact maps - 1).<br /></li><li>cutoff\_0.05: $chi^2$ score cut-off using the $chi^2(df)$ probability distribution curve (5% of the area under the right of the distribution).<br /></li><li>p: p value, i.e., area under the $chi^2(df)$ probability distribution curve, on the right of the chi2 computed value. Warning, the chi2 score used is computed on a single inter contact map (single cell of the matrix) while the $chi^2(df)$ probability distribution curve used is for the sum of all the chi2 scores of all the inter contact maps (chi2 score total). Thus, it is as if the chi2 score total that should be positioned on $chi^2(df)$ to get the p value, is cut into many pieces, all positioned on the same curve to get $n_{c}$ p values. This strongly lower the sensitivity of the tests but increase the specificity.<br /></li><li>signif\_0.05: $p \leq 5\%$ and $res \gt 0$ is indicated by a star.<br /></li><li>signif\_0.05_BF: bonferroni correction of p using the number of inter contact maps $n_{c}$, i.e., $p \times n_{c} \leq 5\%$ and $res \gt 0$, indicated by a star.</li> |


<br /><br />
## VERSIONS


The different releases are tagged [here](https://gitlab.pasteur.fr/gmillot/micmac/-/tags).

<br /><br />
## LICENCE


This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchandability or fitness for a particular purpose.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.

<br /><br />
## CITATION


Not yet published.


<br /><br />
## CREDITS


[Gael A. Millot](https://gitlab.pasteur.fr/gmillot), Bioinformatics and Biostatistics Hub, Institut Pasteur, Paris, France

<br /><br />
## ACKNOWLEDGEMENTS


The developers & maintainers of the mentioned softwares and packages, including:

- [R](https://www.r-project.org/)
- [Nextflow](https://www.nextflow.io/)
- [Apptainer](https://apptainer.org/)
- [Docker](https://www.docker.com/)
- [Gitlab](https://about.gitlab.com/)
- [Bash](https://www.gnu.org/software/bash/)
- [Ubuntu](https://ubuntu.com/)


<br /><br />
## WHAT'S NEW IN


#### v1.0

Everything.


